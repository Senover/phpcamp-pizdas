<?php
class Reservation
{
	public $From ;  
	public $To ; 
	public $Value ; 
	
	function __construct($a, $b, $c){
		$this->From = $a;
		$this->To = $b;
		$this->Value = $c;
	}
	public function __toString()
    {
        return 'Jestem rezerwacja na pokój '.$this->Value.' na termin '.date('d', $this->From). ' do '.date('d',$this->To);
    }
}
$reservation = new Reservation(strtotime("29 May 2017"), strtotime("1 July 2017"), '532');
echo $reservation;

