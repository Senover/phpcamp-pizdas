<?php

/**
 * Created by PhpStorm.
 * User: Daniluk
 * Date: 2017-05-30
 * Time: 11:24
 */
require_once 'Macron.php';

class Product extends Macron
{
    private $weight;
    private $quantity;

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }


}
