<?php

abstract class Macron
{
    protected $id;
    protected $price;
    protected $name;
    protected $currency;
    protected $description;
    protected $pictures;
    protected $producet;
    protected $category;

    public function __set($name, $value)
    {
        echo "Setting '$name' to '$value'\n";
        $this->$name = $value;
    }

    public function __get($name)
    {
        echo "Getting '$name'\n";
        if (array_key_exists($name, $this->data)) {
            return $this->$name;
        }
    }

    public function __call($name, $args)
    {
        //var_dump($name);
        $k=strtolower($name);
        $n = substr($k, 3);
        if (isset($this->$n)) {
            if (isset($args[0]))
                return $this->$n = $args[0];
            return $this->$n;
        }
        return false;

    }
}