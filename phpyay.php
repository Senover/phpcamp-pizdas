<?php
class DB implements iDB {
	private $link;
	private $lastquery;
	

	/**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
	 
	 
    public function __construct($host, $login, $password, $dbName){
	
		$this->link= mysqli_connect($host, $login, $password, $dbName);
	
		if (mysqli_connect_errno())
  {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
	}

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);
	
	$this->lastquery=mysqli_query(link,$query,$resultmode= MYSQLI_STORE_RESULT);
	

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}
	
	

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
	 
	 
    public function __construct($host, $login, $password, $dbName);

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

/*$DB=new DB('localhost','login','hasło','baza');
$DB->query('SELECT * FROM clients LIMIT 1000');
print $DB->getAffectedRows() . '<br>';
var_dump($DB->getROW());
var_dump($DB->getAllRows());*/